# Face recognition with dlib

## What's it.
Test project for face recognition and identification using python3, opencv, dlib and face-recognition libs.

## How to run.

### Step 1.
Create a dataset from 1 or more know faces on ./dataset/<label>/*jpg

### Step 2.
Run
```bash
$ python3 generate.py --dataset ./dataset --encodings ./encodings.pickle
```
This step must create a encodings.pickle file in the project root.

### Step 3.

Use the face encoding base to compare with new sample:

```bash
$ python3 test.py
```

This script must return a ./registers.db in project folder's.


## Results

>   When using a distance threshold of 0.6, the dlib model obtains an accuracy
>   of 99.38% on the standard LFW face recognition benchmark, which is
>   comparable to other state-of-the-art methods for face recognition as of
>   February 2017. This accuracy means that, when presented with a pair of face
>   images, the tool will correctly identify if the pair belongs to the same
>   person or is from different people 99.38% of the time.

> source http://dlib.net/face_recognition.py.html
