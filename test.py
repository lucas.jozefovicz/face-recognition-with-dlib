'''
Sample code
Docs are MarkDown based

by @lucas.jozefovicz on gitlab.
'''

import resource
import face_recognition
import argparse
import pickle
import cv2
import os
import sqlite3
import subprocess
import datetime

'''
### save_face
Input: data to be registered in sql database column by column
Output: Create or insert in ./registers.db file
'''
def save_register(serialNumber, label, horario):

    db_file = './registers.db'
    conn = sqlite3.connect(db_file)

    c = conn.cursor()

    c.execute('''CREATE TABLE IF NOT EXISTS dadospenvio(data TEXT NOT NULL, serial_number INTEGER NOT NULL, id TEXT NOT NULL, flag INTEGER NOT NULL)''')

    registros = [(horario, serialNumber, label, 0), ]

    c.executemany('INSERT INTO dadospenvio VALUES (?, ?, ?, ?)', registros)
    conn.commit()



# parse arguments from terminal
ap = argparse.ArgumentParser()
ap.add_argument("-d", "--detection_method", type=str, default="cnn",
	help="face detection model to use: either `hog` or `cnn`")
args = vars(ap.parse_args())

# load the known faces and embeddings from file
data = pickle.loads(open("encodings.pickle", "rb").read())


# load list of files to test
files = []

for r, d, f in os.walk("./faces/"):
    for file in f:
        if '.jpg' in file:
            files.append(os.path.join(r, file))

files.sort(key=lambda f: os.path.getmtime(f))


# main code
for k in files:
	image = cv2.imread(k, 0)
	rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

	# compute the facial embeddings for each face
	boxes = face_recognition.face_locations(rgb,
		model=args["detection_method"])
	encodings = face_recognition.face_encodings(rgb, boxes)

	# initialize the list of names for each face detected
	names = []

	# loop over the facial embeddings
	for encoding in encodings:

		# try to match each face in the input image to our known encodings
		matches = face_recognition.compare_faces(data["encodings"], encoding, tolerance=0.7)
		name = "None"

		# check to see if we have found a match
		if True in matches:

			matchedIdxs = [i for (i, b) in enumerate(matches) if b]
			counts = {}

			# loop over the matched indexes and maintain a count for
			# each recognized face
			for i in matchedIdxs:
				name = data["names"][i]
				counts[name] = counts.get(name, 0) + 1

			# determine the recognized face with the largest number of votes
			name = max(counts, key=counts.get)
		
		# update the list of names
		names.append(name)

	if(len(names) > 0):
		#horario = k[len(k)-28:len(k)-11]
		horario = str(datetime.datetime.now())
		print(str(k) + " ---> " + str(names[0]))
		save_register(0, names[0], horario)

	# loop over the recognized faces
	for ((top, right, bottom, left), name) in zip(boxes, names):
		# draw the predicted face name on the image
		cv2.rectangle(image, (left, top), (right, bottom), (0, 255, 0), 2)
		y = top - 15 if top - 15 > 15 else top + 15
		cv2.putText(image, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
			0.75, (0, 255, 0), 2)

	# move processed images to /old_faces
	# if this folder was created before
	cv2.imwrite(str(k).replace("faces", "old_faces"), image)
	os.remove(k)
